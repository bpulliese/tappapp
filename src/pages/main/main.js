"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var ionic_native_1 = require('ionic-native');
var settings_1 = require('../settings/settings');
var TapService_1 = require("../../services/TapService");
var tap_model_1 = require("../../models/tap.model");
var tap_model_2 = require("../../models/tap.model");
var StatusService_1 = require("../../services/StatusService");
var status_model_1 = require("../../models/status.model");
var moment = require('moment');
var MainPage = (function () {
    function MainPage(platform, nav, tap, status) {
        var _this = this;
        this.tapRequestSuccessEvent = new core_1.EventEmitter();
        this.tapRequestFailedEvent = new core_1.EventEmitter();
        this.statusRequestSuccessEvent = new core_1.EventEmitter();
        this.statusRequestFailedEvent = new core_1.EventEmitter();
        this.touchPrecentage = 0.0;
        this._liveTouchPercentage = 0;
        this.isCheckingStatus = false;
        this.nextTapType = tap_model_2.TapType.On;
        this.isTapPending = false;
        this.onInit = function () {
            _this.tapRequestSuccessEvent = _this.tapService.tapRequestSuccessEvent
                .subscribe(function (msg) {
                _this.loading.dismiss();
                _this.isTapPending = false;
                _this.getStatusLogs(true);
            });
            _this.tapRequestFailedEvent = _this.tapService.tapRequestFailedEvent
                .subscribe(function (msg) {
                _this.presentToast(msg);
                _this.loading.dismiss();
                _this.isTapPending = false;
            });
            _this.statusRequestSuccessEvent = _this.statusService.statusRequestSuccessEvent
                .subscribe(function (status) {
                _this.displayCurrentStatus(status.last_tap_on, status.last_tap_off);
                _this.loading.dismiss();
                _this.nextTapType = (status.last_tap_on != "") ? tap_model_2.TapType.Off : tap_model_2.TapType.On;
                //tap exceeded dialog
                if (status.last_tap_on != "" && status.last_tap_off != "") {
                    _this.showTapExceededDialog();
                    _this.isTapPending = false;
                }
                else {
                    if (_this.isTapPending) {
                        if (_this.nextTapType == tap_model_2.TapType.Off) {
                            _this.showTapOffConfirm();
                        }
                        else {
                            setTimeout(function () {
                                _this.sendTapRequest();
                            }, 2000); //great thought we would get rid of the need for timeouts in angular2
                        }
                        _this.isTapPending = false;
                    }
                }
            });
            _this.statusRequestFailedEvent = _this.statusService.statusRequestFailedEvent
                .subscribe(function (msg) {
                _this.presentToast(msg);
                _this.loading.dismiss();
            });
            //make first handshake and get tap logs
            _this.getStatusLogs();
        };
        this.nav = nav;
        this.tapService = tap;
        this.statusService = status;
        this.local = this.local = new ionic_angular_1.Storage(ionic_angular_1.LocalStorage);
        platform.ready().then(function () {
            _this.onInit();
            _this.configureProgressBar();
        });
        /**
         * The resume event emits when the native platform pulls the application out from the background.
         * This event would emit when a Cordova app comes out from the background, however, it would not
         * fire on a standard web browser.
         */
        document.addEventListener('resume', function () {
            _this.getStatusLogs();
        });
    }
    Object.defineProperty(MainPage.prototype, "formattedTouchPercentage", {
        get: function () {
            return (this._liveTouchPercentage == 0) ? "ready..." : this._liveTouchPercentage + ' seconds';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MainPage, "parameters", {
        get: function () {
            return [[ionic_angular_1.Platform], [ionic_angular_1.NavController], [TapService_1.TapService], [StatusService_1.StatusService]];
        },
        enumerable: true,
        configurable: true
    });
    MainPage.prototype.ngOnDestroy = function () {
    };
    MainPage.prototype.onTouchStart = function () {
        var _this = this;
        console.log('touch start');
        this._animateFingerCalibrationTimer = setInterval(function () { _this.animateFingerCalibration(); }, 1000);
    };
    MainPage.prototype.onTouchEnd = function () {
        console.log('touch end');
        //reset touch percentage
        clearInterval(this._animateFingerCalibrationTimer);
        this.touchPrecentage = 0.0;
        this.bar.animate(this.touchPrecentage);
    };
    MainPage.prototype.animateFingerCalibration = function () {
        this.touchPrecentage = (this.touchPrecentage < 1.0) ? this.touchPrecentage + 0.1 : 1.0;
        this.bar.animate(this.touchPrecentage);
        if (this.touchPrecentage >= 1.0) {
            this.onTouchEnd();
            this.isTapPending = true;
            this.getStatusLogs();
        }
    };
    MainPage.prototype.showTapOffConfirm = function () {
        var _this = this;
        var confirm = ionic_angular_1.Alert.create({
            title: 'Confirmation Required',
            message: 'Are you sure you want to tap off?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Confirm',
                    handler: function () {
                        _this.sendTapRequest();
                    }
                }
            ]
        });
        this.nav.present(confirm);
    };
    MainPage.prototype.showTapExceededDialog = function () {
        var dialog = ionic_angular_1.Alert.create({
            title: 'Information',
            message: 'You have tapped on & off today.',
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                    }
                }
            ]
        });
        this.nav.present(dialog);
    };
    MainPage.prototype.sendTapRequest = function () {
        var _this = this;
        var currentdate = new Date();
        var datetime = currentdate.getFullYear() + "/"
            + (currentdate.getMonth() + 1) + "/"
            + currentdate.getDate() + " " +
            +currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
        var tapDetails = new tap_model_1.TapDetails(datetime);
        this.presentLoadingDefault("Sending tap request...");
        console.log("get geo location");
        ionic_native_1.Geolocation.getCurrentPosition().then(function (pos) {
            console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
            tapDetails.latitude = pos.coords.latitude;
            tapDetails.longitude = pos.coords.longitude;
            _this.tapService.sendTap(tapDetails);
        });
    };
    MainPage.prototype.getStatusLogs = function (silent) {
        var _this = this;
        if (silent === void 0) { silent = false; }
        if (!this.isCheckingStatus) {
            this.isCheckingStatus = true;
            this.local.get('settings').then(function (data) {
                if (data) {
                    if (!silent)
                        _this.presentLoadingDefault("Checking your tap activity, please wait...");
                    _this.statusService.requestStatus();
                }
                else {
                    _this.nav.push(settings_1.SettingsPage);
                }
                _this.isCheckingStatus = false;
            });
        }
    };
    MainPage.prototype.presentLoadingDefault = function (msg) {
        if (this.loading)
            this.loading.dismiss();
        this.loading = ionic_angular_1.Loading.create({
            content: msg
        });
        this.nav.present(this.loading);
    };
    MainPage.prototype.presentToast = function (msg) {
        if (msg === void 0) { msg = "Something went wrong."; }
        var toast = ionic_angular_1.Toast.create({
            message: msg,
            showCloseButton: true
        });
        this.nav.present(toast);
    };
    MainPage.prototype.configureProgressBar = function () {
        var _this = this;
        this.bar = new ProgressBar.SemiCircle("#tapIndicator", {
            strokeWidth: 6,
            color: '#FFEA82',
            trailColor: '#eee',
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            svgStyle: null,
            text: false,
            from: { color: '#FFEA82' },
            to: { color: '#ED6A5A' },
            // Set default step function for all animate calls
            step: function (state, bar) {
                bar.path.setAttribute('stroke', state.color);
                _this._liveTouchPercentage = Math.round(bar.value() * 5);
            }
        });
    };
    MainPage.prototype.displayCurrentStatus = function (tapOnLog, tapOffLog) {
        var on = (tapOnLog != "") ? moment(tapOnLog).format('LTS') : "", off = (tapOffLog != "") ? moment(tapOffLog).format('LTS') : "";
        if (on != "" || off != "")
            this.status = new status_model_1.Status(on, off);
    };
    MainPage.prototype.goToSettings = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.nav.push(settings_1.SettingsPage);
    };
    MainPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/main/main.html'
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.Platform, ionic_angular_1.NavController, TapService_1.TapService, StatusService_1.StatusService])
    ], MainPage);
    return MainPage;
}());
exports.MainPage = MainPage;
//# sourceMappingURL=main.js.map