import {Component,EventEmitter} from '@angular/core';
import {
  NavController, Loading, Platform,
  AlertController, ToastController, LoadingController
} from 'ionic-angular';
import {SettingsPage} from '../settings/settings';
import {TapService} from "../../services/TapService";
import {TapDetails} from "../../models/tap.model";
import {TapType} from "../../models/tap.model";
import {StatusService} from "../../services/StatusService";
import {Status} from "../../models/status.model";
import * as moment from 'moment';
import {Geolocation} from "@ionic-native/geolocation";
import {Storage} from "@ionic/storage";
const ProgressBar = require('progressbar.js');

@Component({
    selector: 'page-main',
    templateUrl: './main.html'
})
export class MainPage
{

    private bar:any;
    private tapRequestSuccessEvent: EventEmitter<string> = new EventEmitter<string>();
    private tapRequestFailedEvent: EventEmitter<string> = new EventEmitter<string>();
    private statusRequestSuccessEvent: EventEmitter<any> = new EventEmitter<any>();
    private statusRequestFailedEvent: EventEmitter<string> = new EventEmitter<string>();
    private loading:Loading;
    private touchPrecentage:number = 0.0;
    public status:Status;
    private _liveTouchPercentage:number = 0;
    private _animateFingerCalibrationTimer;
    private isCheckingStatus:boolean = false;
    private nextTapType:TapType = TapType.On;
    private isTapPending:boolean = false;


    get formattedTouchPercentage():string{
        return (this._liveTouchPercentage == 0)? "ready..." : this._liveTouchPercentage+ ' seconds';
    }

    constructor(private platform:Platform,
                private nav: NavController,
                private tapService:TapService,
                private statusService:StatusService,
                private alert: AlertController,
                private location: Geolocation,
                private storage: Storage,
                private toastCtrl: ToastController,
                private loadingCtrl: LoadingController){
        this.platform.ready().then(() => {
            this.onInit();
            this.configureProgressBar();
        });

        /**
         * The resume event emits when the native platform pulls the application out from the background.
         * This event would emit when a Cordova app comes out from the background, however, it would not
         * fire on a standard web browser.
         */
        document.addEventListener('resume', () => {
            this.getStatusLogs();
        });

    }


    onInit = () =>{


        this.tapRequestSuccessEvent = this.tapService.tapRequestSuccessEvent
            .subscribe(msg => {
                this.isTapPending = false;
                this.getStatusLogs(true);
            });

        this.tapRequestFailedEvent = this.tapService.tapRequestFailedEvent
            .subscribe(msg => {
                this.presentToast(msg);
                this.isTapPending = false;
            });

        this.statusRequestSuccessEvent = this.statusService.statusRequestSuccessEvent
            .subscribe(status => {
                this.displayCurrentStatus(status.last_tap_on, status.last_tap_off);
                this.nextTapType = (status.last_tap_on != "") ? TapType.Off : TapType.On;


                //tap exceeded dialog
                if(status.last_tap_on!="" && status.last_tap_off!="") {
                    this.showTapExceededDialog();
                    this.isTapPending = false;
                }
                else
                {
                    if (this.isTapPending) {
                        if (this.nextTapType == TapType.Off) {
                            this.showTapOffConfirm();
                        } else {
                            setTimeout( () => {
                                this.sendTapRequest();
                            }, 2000); //great thought we would get rid of the need for timeouts in angular2

                        }
                        this.isTapPending = false;
                    }
                }


            });

        this.statusRequestFailedEvent = this.statusService.statusRequestFailedEvent
            .subscribe(msg => {
                this.presentToast(msg);
            });

        //make first handshake and get tap logs
        this.getStatusLogs();
    };

    ngOnDestroy() {

    }


    onTouchStart(){
        console.log('touch start');
        this._animateFingerCalibrationTimer = setInterval(()=>{ this.animateFingerCalibration() }, 1000);
    }

    onTouchEnd(){
        console.log('touch end');
        //reset touch percentage
        clearInterval(this._animateFingerCalibrationTimer);
        this.touchPrecentage = 0.0;
        this.bar.animate(this.touchPrecentage);
    }

    private animateFingerCalibration(){

        this.touchPrecentage = (this.touchPrecentage < 1.0) ? this.touchPrecentage + 0.1 : 1.0;
        this.bar.animate(this.touchPrecentage);
        if (this.touchPrecentage >= 1.0) {
            this.onTouchEnd();
            this.isTapPending = true;
            this.getStatusLogs();
        }
    }

    private showTapOffConfirm() {
        let confirm = this.alert.create({
            title: 'Confirmation Required',
            message: 'Are you sure you want to tap off?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Confirm',
                    handler: () => {
                        this.sendTapRequest();
                    }
                }
            ]
        });
      confirm.present();
    }


    private showTapExceededDialog() {
        let dialog = this.alert.create({
            title: 'Information',
            message: 'You have tapped on & off today.',
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {

                    }
                }
            ]
        });
      dialog.present();
    }

    private sendTapRequest(){
        let currentdate = new Date();
        let datetime = currentdate.getFullYear() + "/"
            + (currentdate.getMonth()+1) + "/"
            + currentdate.getDate() + " " +
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
        let tapDetails = new TapDetails(datetime);
        this.presentLoadingDefault("Sending tap request...");

        console.log("get geo location");

        this.location.getCurrentPosition().then(pos => {
            console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
            tapDetails.latitude = pos.coords.latitude;
            tapDetails.longitude = pos.coords.longitude;
            this.tapService.sendTap(tapDetails);
        });

    }

    private getStatusLogs(silent:boolean = false){
        if(!this.isCheckingStatus) {
            this.isCheckingStatus = true;
            this.storage.get('settings').then((data) => {
                if (data) {
                    if (!silent)
                        this.presentLoadingDefault("Checking your tap activity, please wait...");

                    this.statusService.requestStatus();
                } else {
                    this.nav.push(SettingsPage);
                }
                this.isCheckingStatus = false;
            });
        }
    }


    presentLoadingDefault(msg:string) {

        this.loading = this.loadingCtrl.create({
            content: msg,
            duration: 3000
        });

        this.loading.present();
    }

    presentToast(msg:string = "Something went wrong.") {
        let toast = this.toastCtrl.create({
            message: msg,
            showCloseButton:true
        });

        toast.present();
    }


    private configureProgressBar() {

        this.bar = new ProgressBar.SemiCircle("#tapIndicator", {
            strokeWidth: 6,
            color: '#FFEA82',
            trailColor: '#eee',
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            svgStyle: null,
            text: false,
            from: {color: '#FFEA82'},
            to: {color: '#ED6A5A'},
            // Set default step function for all animate calls
            step: (state, bar) => {
                bar.path.setAttribute('stroke', state.color);
                this._liveTouchPercentage = Math.round(bar.value() * 5);
            }
        });

    }

    displayCurrentStatus(tapOnLog:string,tapOffLog:string){

        var on = (tapOnLog!="")? moment(tapOnLog).format('LTS') : "",
            off = (tapOffLog!="")? moment(tapOffLog).format('LTS') : "";

        if(on!="" || off!="")
            this.status = new Status(on, off);

    }

    goToSettings(){
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.nav.push(SettingsPage);
    }
}
