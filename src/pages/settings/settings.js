"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_angular_1 = require('ionic-angular');
var credentials_model_1 = require("../../models/credentials.model");
var AuthenticationService_1 = require("../../services/AuthenticationService");
var ionic_native_1 = require('ionic-native');
var SettingsPage = (function () {
    function SettingsPage(platform, auth, nav) {
        var _this = this;
        this.credentials = new credentials_model_1.Credentials("", "");
        this.auth = auth;
        this.local = new ionic_angular_1.Storage(ionic_angular_1.LocalStorage);
        this.nav = nav;
        platform.ready().then(function () {
            _this.getSavedSettings();
            _this.getGeolocation();
        });
    }
    Object.defineProperty(SettingsPage, "parameters", {
        get: function () {
            return [[ionic_angular_1.Platform], [AuthenticationService_1.AuthenticationService], [ionic_angular_1.NavController]];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SettingsPage.prototype, "geolocation", {
        get: function () {
            return this._geolocation;
        },
        enumerable: true,
        configurable: true
    });
    SettingsPage.prototype.getGeolocation = function () {
        var _this = this;
        ionic_native_1.Geolocation.getCurrentPosition().then(function (pos) {
            console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
            _this._geolocation = 'lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude;
        });
    };
    SettingsPage.prototype.getSavedSettings = function () {
        var _this = this;
        this.local.get('settings').then(function (data) {
            if (data)
                _this.credentials = JSON.parse(data);
        });
    };
    SettingsPage.prototype.onSaveSettings = function () {
        this.local.set('settings', JSON.stringify(this.credentials));
        this.presentToast('Credentials saved!');
    };
    SettingsPage.prototype.presentToast = function (msg) {
        if (msg === void 0) { msg = "Something went wrong."; }
        var toast = ionic_angular_1.Toast.create({
            message: msg,
            showCloseButton: true
        });
        this.nav.present(toast);
    };
    /**
     * On authenticate save token to localStorgae
     */
    SettingsPage.prototype.onAuthenticate = function () {
        var _this = this;
        this.auth.authenticate(this.credentials)
            .subscribe(function (data) {
            if (data.success) {
                _this.local.set('token', data.token);
                _this.presentToast('Authenticated successfully!');
            }
            else {
                _this.presentToast(data.msg);
            }
        }, function (e) {
            _this.presentToast("Blogix server is not responding. Please try again later.");
        });
    };
    SettingsPage = __decorate([
        ionic_angular_1.Page({
            templateUrl: 'build/pages/settings/settings.html'
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.Platform, AuthenticationService_1.AuthenticationService, ionic_angular_1.NavController])
    ], SettingsPage);
    return SettingsPage;
}());
exports.SettingsPage = SettingsPage;
//# sourceMappingURL=settings.js.map