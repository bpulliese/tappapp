import { Platform, ToastController} from 'ionic-angular';
import {Credentials} from "../../models/credentials.model";
import {AuthenticationService} from "../../services/AuthenticationService";
import {Geolocation} from "@ionic-native/geolocation";
import {Component} from "@angular/core";
import {Storage} from "@ionic/storage";


@Component({
    selector: 'page-settings',
    templateUrl: './settings.html'
})

export class SettingsPage {
    public credentials:Credentials= new Credentials("","");
    private _geolocation:string;


    constructor(private platform:Platform,
                private auth:AuthenticationService,
                private location: Geolocation,
                private storage: Storage,
                private toastCtrl: ToastController) {
        this.platform.ready().then(() => {
            this.getSavedSettings();
            this.getGeolocation();
        });
    }

    get geolocation():string{
       return this._geolocation;
    }

    getGeolocation(){

      this.location.getCurrentPosition().then(pos => {
        console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
        this._geolocation = 'lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude;
      });

      const watch = this.location.watchPosition().subscribe(pos => {
        console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
        this._geolocation = 'lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude;
      });

      // to stop watching
      watch.unsubscribe();
    }

    getSavedSettings(){
        this.storage.get('settings').then((data) => {
                if(data)
                    this.credentials = JSON.parse(data);
        });
    }

    onSaveSettings(){
        this.storage.set('settings', JSON.stringify(this.credentials));
        this.presentToast('Credentials saved!');
    }


    presentToast(msg:string = "Something went wrong.") {
        let toast = this.toastCtrl.create({
            message: msg,
            showCloseButton:true
        });

      toast.present();
    }

    /**
     * On authenticate save token to localStorgae
     */
    onAuthenticate(){
        this.auth.authenticate(this.credentials)
            .subscribe((data: any) => {
                if(data.success) {
                    this.storage.set('token', data.token).then(()=>{
                      this.presentToast('Authenticated successfully!');
                    });
                }else {
                    this.presentToast(data.msg);
                }
            }, (e)=>{
                this.presentToast("Blogix server is not responding. Please try again later.");
            });
    }

}


