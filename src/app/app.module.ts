import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import {Geolocation} from "@ionic-native/geolocation";
import {IonicStorageModule} from "@ionic/storage";
import {MainPage} from "../pages/main/main";
import {TapService} from "../services/TapService";
import {AuthenticationService} from "../services/AuthenticationService";
import {StatusService} from "../services/StatusService";
import {HttpClientModule} from "@angular/common/http";
import {SettingsPage} from "../pages/settings/settings";

@NgModule({
  declarations: [
    MyApp,
    MainPage,
    SettingsPage
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__tapp_app_db',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainPage,
    SettingsPage
  ],
  providers: [
    TapService,
    AuthenticationService,
    StatusService,
    Geolocation,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
