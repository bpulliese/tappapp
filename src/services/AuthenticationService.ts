import {Injectable} from '@angular/core';
import {Credentials} from "../models/credentials.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {_throw} from "rxjs/observable/throw";
import {catchError} from "rxjs/operators";

let authenticateURL = 'https://blogix.pacificbmg.com.au/api/v1/authenticate.php';

@Injectable()
export class AuthenticationService {

    constructor (private http:HttpClient) {
    }

    authenticate(auth:Credentials) {
        let body = JSON.stringify(auth);
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
        return this.http.post(authenticateURL, body, httpOptions)
            .pipe(
              catchError(this.handleError)
            );
    }

    handleError(error) {
        return (error.json())? _throw(error.json().error || 'Server error') : _throw(error.message || 'Server error');
    }

}
