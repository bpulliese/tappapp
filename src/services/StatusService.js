"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
//import {Observable} from 'rxjs/Observable';
var Rx_1 = require('rxjs/Rx');
var AuthenticationService_1 = require("./AuthenticationService");
var ionic_angular_1 = require('ionic-angular');
var tapURL = 'https://blogix.pacificbmg.com.au/api/v1/status.php';
var StatusService = (function () {
    function StatusService(http, auth) {
        this.statusRequestSuccessEvent = new core_1.EventEmitter();
        this.statusRequestFailedEvent = new core_1.EventEmitter();
        this.http = http;
        this.auth = auth;
        this.local = new ionic_angular_1.Storage(ionic_angular_1.LocalStorage);
    }
    /**
     * Status request.
     * Checks for a valid token and sends the request.
     * If no token exists we attempt to log them in.
     */
    StatusService.prototype.requestStatus = function () {
        var _this = this;
        //get token
        this.local.get('token').then(function (data) {
            //if token found send tap request
            if (data) {
                var token = data;
                _this.sendStatusRequest(token)
                    .subscribe(function (data) { return _this.onGetStatusHandler(data); });
            }
            else {
                // on authentication success, we can try to get status again...
                _this.local.get('settings').then(function (data) {
                    if (data) {
                        var credentials = JSON.parse(data);
                        _this.auth.authenticate(credentials)
                            .subscribe(function (data) {
                            if (data.success) {
                                //save token to localStorage
                                _this.local.set('token', data.token);
                                //get status
                                _this.sendStatusRequest(data.token)
                                    .subscribe(function (data) { return _this.onGetStatusHandler(data); });
                            }
                            else {
                                _this.onGetStatusHandler(data);
                            }
                        }, function (e) {
                            _this.statusRequestFailedEvent.emit("Blogix server is not responding. Please try again later.");
                        });
                    }
                });
            }
        });
    };
    /**
     * Handle status request response.
     * Allows 1 retry if token is expired.
     * @param response
     */
    StatusService.prototype.onGetStatusHandler = function (response) {
        var _this = this;
        if (response.success)
            this.statusRequestSuccessEvent.emit(response);
        else if (response.expired_token)
            this.local.remove('token').then(function () {
                _this.requestStatus();
            });
        else
            this.statusRequestFailedEvent.emit(response.msg);
    };
    /**
     * Send a request for logs
     * @param token
     * @returns {Promise<R>|Observable<R>|any}
     */
    StatusService.prototype.sendStatusRequest = function (token) {
        var headers = new http_1.Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.get(tapURL, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    /**
     * Generic error handler for http requests
     * @param error
     * @returns {ErrorObservable}
     */
    StatusService.prototype.handleError = function (error) {
        console.error(error);
        return Rx_1.Observable.throw(error.json().error || 'Server error');
    };
    StatusService = __decorate([
        core_1.Injectable(),
        __metadata('design:paramtypes', [http_1.Http, AuthenticationService_1.AuthenticationService])
    ], StatusService);
    return StatusService;
}());
exports.StatusService = StatusService;
//# sourceMappingURL=StatusService.js.map
