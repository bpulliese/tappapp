"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
//import {Observable} from 'rxjs/Observable';
var Rx_1 = require('rxjs/Rx');
var AuthenticationService_1 = require("./AuthenticationService");
var ionic_angular_1 = require('ionic-angular');
var tapURL = 'https://blogix.pacificbmg.com.au/api/v1/tap.php';
var TapService = (function () {
    function TapService(http, auth, events) {
        this.tapRequestSuccessEvent = new core_1.EventEmitter();
        this.tapRequestFailedEvent = new core_1.EventEmitter();
        this.http = http;
        this.auth = auth;
        this.local = new ionic_angular_1.Storage(ionic_angular_1.LocalStorage);
        this.events = events;
    }
    /**
     * Send tap request.
     * Checks for a valid token and sends the request.
     * If no token exists we attempt to log them in.
     * @param tapDetails
     */
    TapService.prototype.sendTap = function (tapDetails) {
        var _this = this;
        //get token
        this.local.get('token').then(function (data) {
            //if token found send tap request
            if (data) {
                var token = data;
                console.log('sending tap request');
                _this.sendTapRequest(token, tapDetails)
                    .subscribe(function (data) {
                    _this.onSendTapRequestHandler(tapDetails, data);
                }, function (e) {
                    _this.tapRequestFailedEvent.emit("Blogix server is not responding. Please try again later.");
                });
            }
            else {
                // on authentication success, we can try to send the tap request again...
                _this.local.get('settings').then(function (data) {
                    if (data) {
                        var credentials = JSON.parse(data);
                        _this.auth.authenticate(credentials)
                            .subscribe(function (data) {
                            if (data.success) {
                                //save token to localStorage
                                _this.local.set('token', data.token);
                                //send tap request
                                _this.sendTapRequest(data.token, tapDetails)
                                    .subscribe(function (data) { return _this.onSendTapRequestHandler(tapDetails, data); });
                            }
                        }, function (e) {
                            _this.tapRequestFailedEvent.emit("Blogix server is not responding. Please try again later.");
                        });
                    }
                });
            }
        });
    };
    /**
     * Handle tap request response.
     * Allows 1 retry if token is expired.
     * @param request
     * @param response
     */
    TapService.prototype.onSendTapRequestHandler = function (request, response) {
        var _this = this;
        if (response.success)
            this.tapRequestSuccessEvent.emit('success');
        else if (response.expired_token)
            this.local.remove('token').then(function () {
                _this.sendTap(request);
            });
        else
            this.tapRequestFailedEvent.emit(response.msg);
    };
    /**
     * Send tap request to blogix server
     * @param token
     * @param tapDetails
     * @returns {Promise<R>|Observable<R>|any}
     */
    TapService.prototype.sendTapRequest = function (token, tapDetails) {
        var body = JSON.stringify(tapDetails);
        var headers = new http_1.Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(tapURL, body, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    /**
     * Generic error handler for http requests
     * @param error
     * @returns {ErrorObservable}
     */
    TapService.prototype.handleError = function (error) {
        console.error(error);
        return Rx_1.Observable.throw(error.json().error || 'Server error');
    };
    TapService = __decorate([
        core_1.Injectable(),
        __metadata('design:paramtypes', [http_1.Http, AuthenticationService_1.AuthenticationService, ionic_angular_1.Events])
    ], TapService);
    return TapService;
}());
exports.TapService = TapService;
//# sourceMappingURL=TapService.js.map
