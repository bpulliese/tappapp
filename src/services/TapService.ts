import {Injectable, EventEmitter} from '@angular/core';
import {Credentials} from "../models/credentials.model";
import {AuthenticationService} from "./AuthenticationService";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Events} from "ionic-angular";
import {_throw} from "rxjs/observable/throw";
import {Storage} from "@ionic/storage";
import {catchError} from "rxjs/operators";


let tapURL = 'https://blogix.pacificbmg.com.au/api/v1/tap.php';

@Injectable()
export class TapService {

    public tapRequestSuccessEvent: EventEmitter<string> = new EventEmitter<string>();
    public tapRequestFailedEvent: EventEmitter<string> = new EventEmitter<string>();

    constructor (private http:HttpClient,
                 private auth:AuthenticationService,
                 private storage: Storage) {
    }


    /**
     * Send tap request.
     * Checks for a valid token and sends the request.
     * If no token exists we attempt to log them in.
     * @param tapDetails
     */
    sendTap(tapDetails) {
        //get token
        this.storage.get('token').then((data) => {

            //if token found send tap request
            if(data) {
                let token = data;
                console.log('sending tap request');
                this.sendTapRequest(token,tapDetails)
                    .subscribe(data=> {
                        this.onSendTapRequestHandler(tapDetails, data);
                    }, (e)=>{
                        this.tapRequestFailedEvent.emit("Blogix server is not responding. Please try again later.");
                    });
            }
            else
            {   // if token missing, lets try authenticate first with the saved credentials and retrieve a new token.
                // on authentication success, we can try to send the tap request again...
                this.storage.get('settings').then((data) => {
                    if(data) {
                        let credentials:Credentials = JSON.parse(data);
                        this.auth.authenticate(credentials)
                            .subscribe((data: any) => {
                                if(data.success)
                                {
                                    //save token to localStorage
                                    this.storage.set('token', data.token);

                                    //send tap request
                                    this.sendTapRequest(data.token, tapDetails)
                                         .subscribe(data=> this.onSendTapRequestHandler(tapDetails, data));
                                }
                            }, (e)=>{
                                this.tapRequestFailedEvent.emit("Blogix server is not responding. Please try again later.");
                            });
                    }
                });

            }
        });
    }

    /**
     * Handle tap request response.
     * Allows 1 retry if token is expired.
     * @param request
     * @param response
     */
    onSendTapRequestHandler(request, response){
        if(response.success)
            this.tapRequestSuccessEvent.emit('success');
        else
        if(response.expired_token)
            this.storage.remove('token').then(() => {
                this.sendTap(request);
            });
        else
            this.tapRequestFailedEvent.emit(response.msg);
    }

    /**
     * Send tap request to blogix server
     * @param token
     * @param tapDetails
     * @returns {Promise<R>|Observable<R>|any}
     */
    private sendTapRequest(token:string, tapDetails){
        let body = JSON.stringify(tapDetails);
      const httpOptions = {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': token
        })
      };
        return this.http.post(tapURL, body, httpOptions)
          .pipe(
            catchError(this.handleError)
          );
    }


    /**
     * Generic error handler for http requests
     * @param error
     * @returns {ErrorObservable}
     */
    handleError(error) {
        console.error(error);
        return _throw(error.json().error || 'Server error');
    }

}
