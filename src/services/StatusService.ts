import {Injectable, EventEmitter} from '@angular/core';
import {Credentials} from "../models/credentials.model";
import {AuthenticationService} from "./AuthenticationService";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import {catchError} from "rxjs/operators";
import {_throw} from "rxjs/observable/throw";


let tapURL = 'https://blogix.pacificbmg.com.au/api/v1/status.php';

@Injectable()
export class StatusService {


    public statusRequestSuccessEvent: EventEmitter<string> = new EventEmitter<string>();
    public statusRequestFailedEvent: EventEmitter<string> = new EventEmitter<string>();

    constructor (private http:HttpClient, private auth:AuthenticationService, private storage: Storage) {

    }


    /**
     * Status request.
     * Checks for a valid token and sends the request.
     * If no token exists we attempt to log them in.
     */
    requestStatus() {
        //get token
        this.storage.get('token').then((data) => {

            //if token found send tap request
            if(data) {
                let token = data;
                this.sendStatusRequest(token)
                    .subscribe(data=> this.onGetStatusHandler(data));
            }
            else
            {   // if token missing, lets try authenticate first with the saved credentials and retrieve a new token.
                // on authentication success, we can try to get status again...
                this.storage.get('settings').then((data) => {
                    if(data) {
                        let credentials:Credentials = JSON.parse(data);
                        this.auth.authenticate(credentials)
                            .subscribe((data: any) => {
                                if(data.success)
                                {
                                    //save token to localStorage
                                    this.storage.set('token', data.token);

                                    //get status
                                    this.sendStatusRequest(data.token)
                                        .subscribe(data=> this.onGetStatusHandler(data));
                                }else{
                                    this.onGetStatusHandler(data)
                                }
                            }, (e)=>{
                                this.statusRequestFailedEvent.emit("Blogix server is not responding. Please try again later.");
                            });
                    }
                });

            }
        });
    }

    /**
     * Handle status request response.
     * Allows 1 retry if token is expired.
     * @param response
     */
    onGetStatusHandler(response){
        if(response.success)
            this.statusRequestSuccessEvent.emit(response);
        else
        if(response.expired_token)
            this.storage.remove('token').then(() => {
                this.requestStatus();
            });
        else
            this.statusRequestFailedEvent.emit(response.msg);
    }

    /**
     * Send a request for logs
     * @param token
     * @returns {Promise<R>|Observable<R>|any}
     */
    private sendStatusRequest(token:string){
      const httpOptions = {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': token
        })
      };

        return this.http.get(tapURL, httpOptions)
          .pipe(
            catchError(this.handleError)
          );
    }


    /**
     * Generic error handler for http requests
     * @param error
     * @returns {ErrorObservable}
     */
    handleError(error) {
        console.error(error);
        return _throw(error.json().error || 'Server error');
    }

}
