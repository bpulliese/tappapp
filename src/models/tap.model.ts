export enum TapType{
    On,
    Off
}

export class TapDetails {
    constructor(public datetime:string, public latitude:number = 0.0, public longitude:number = 0.0, public reverse_geocode:string = "Bamuda Triangle"){

    }
}
