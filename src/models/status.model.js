"use strict";
var Status = (function () {
    function Status(on, off) {
        this.on = on;
        this.off = off;
    }
    return Status;
}());
exports.Status = Status;
//# sourceMappingURL=status.model.js.map