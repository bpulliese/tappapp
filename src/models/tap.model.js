"use strict";
(function (TapType) {
    TapType[TapType["On"] = 0] = "On";
    TapType[TapType["Off"] = 1] = "Off";
})(exports.TapType || (exports.TapType = {}));
var TapType = exports.TapType;
var TapDetails = (function () {
    function TapDetails(datetime, latitude, longitude, reverse_geocode) {
        if (latitude === void 0) { latitude = "0.0"; }
        if (longitude === void 0) { longitude = "0.0"; }
        if (reverse_geocode === void 0) { reverse_geocode = "Bamuda Triangle"; }
        this.datetime = datetime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.reverse_geocode = reverse_geocode;
    }
    return TapDetails;
}());
exports.TapDetails = TapDetails;
//# sourceMappingURL=tap.model.js.map